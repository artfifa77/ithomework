import requests
import time

bank_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'


def get_json():

    response_json = ''

    try:
        response = requests.request('GET', bank_url)
    except ConnectionError:
        print('exception')
    else:
        response_json = response.json()

    return response_json


def get_currency_name(response_json):
    """

    Function gets currency names from .json file and forms list with them

    Returns: List with currency names

    """
    currency_name_list = []

    for currency_name in response_json:
        currency_name = currency_name.get('cc', None)
        currency_name_list.append(currency_name)

    return currency_name_list


def get_currency_value(response_json):
    """

    Function gets currency values from .json file and forms list with them

    Returns: List with currency values

    """
    value_list = []

    for currency_value in response_json:
        currency_value = currency_value.get('rate', None)
        value_list.append(round(currency_value, 4))

    return value_list


def get_currency_dict(currency_name, currency_value):
    """

    Args:
        currency_name (list): List with currency names
        currency_value (list): List with currency values

    Returns: Dictionary, which combines list with currency names and list with currency values

    """
    return dict(zip(currency_name, currency_value))


def currency_data_maker(currency_dict):
    """

    Function forms a string in the format 'Index. Currency name - Currency value' with date,
    writes to file 'database.txt'

    Args:
        currency_dict (dict): Dict with currency names

    """
    date = time.strftime("%d %b %Y")

    idx = 1

    with open('database.txt', 'w') as out:

        out.write(f'{date}\n')

        for currency_name, currency_value in currency_dict.items():
            currency_msg = '{}. {} - {}\n'.format(idx, currency_name, currency_value)
            out.write(currency_msg)
            idx += 1


def main():

    response_json = get_json()

    currency_name = get_currency_name(response_json)

    currency_value = get_currency_value(response_json)

    currency_dict = get_currency_dict(currency_name, currency_value)

    currency_data_maker(currency_dict)

main()
