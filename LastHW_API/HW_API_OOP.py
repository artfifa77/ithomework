import requests
import time


class Response:

    response_json = ''

    bank_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

    try:
        response = requests.request('GET', bank_url)
    except ConnectionError:
        print('exception')
    else:
        response_json = response.json()


class CurrencyData(Response):

    option = ''

    def __init__(self, json_option):
        self.options_list = []
        self.option = json_option

    @property
    def options(self):
        return self.option

    @options.setter
    def options(self, json_option):
        allowed_options = ['txt', 'cc', 'rate']
        if json_option not in allowed_options:
            raise TypeError
        self.options = json_option

    @property
    def list_maker(self):
        for item in Response.response_json:
            item = item.get(self.option, None)
            self.options_list.append(item)
        return self.options_list


class CurrencyDict:

    name_list = []
    value_list = []

    def __init__(self, new_name_list, new_value_list):
        self.name_list = new_name_list
        self.value_list = new_value_list

    @property
    def currency_name_list(self):
        return self.name_list

    @currency_name_list.setter
    def currency_name_list(self, new_name_list):
        if not isinstance(new_name_list, list):
            raise TypeError
        self.name_list = new_name_list

    @property
    def currency_value_list(self):
        return self.value_list

    @currency_value_list.setter
    def currency_value_list(self, new_value_list):
        if not isinstance(new_value_list, list):
            raise TypeError
        self.value_list = new_value_list

    @property
    def zipper(self):
        return dict(zip(self.name_list, self.value_list))


def currency_data_maker(currency):

    date = time.strftime("%d %b %Y")

    idx = 1

    with open('database.txt', 'w') as out:

        out.write(f'{date}\n')

        for name, value in currency.items():
            currency_msg = '{}. {} - {}\n'.format(idx, name, value)
            out.write(currency_msg)
            idx += 1


currency_name = CurrencyData('cc')
currency_value = CurrencyData('rate')

currency_dict = CurrencyDict(currency_name.list_maker, currency_value.list_maker)

currency_data_maker(currency_dict.zipper)
