class Vehicle:
    engine = True


class Car(Vehicle):
    color = ""
    max_speed = ""

    def __init__(self, initial_color, initial_max_speed):
        self.color = initial_color
        self.max_speed = initial_max_speed


class Plane(Vehicle):
    passenger_capacity = ""
    belonging = ""

    def __init__(self, initial_passenger_capacity, initial_belonging):
        self.passenger_capacity = initial_passenger_capacity
        self.belonging = initial_belonging


class Ship(Vehicle):
    length = ""
    ship_type = ""

    def __init__(self, initial_length, initial_ship_type):
        self.length = initial_length
        self.ship_type = initial_ship_type


Bugatti_Veyron = Car(initial_color="black", initial_max_speed="410 km/h")

Boeing_747 = Plane(initial_passenger_capacity="550 passengers", initial_belonging="civil")

Sheherazade = Ship(initial_length="140 meters", initial_ship_type="yacht")
