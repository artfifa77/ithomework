from random import choice
import time


WIN_RULES = {
    'Rock': ('Scissors', 'Spock'),
    'Paper': ('Rock', 'Spock'),
    'Scissors': ('Paper', 'Lizard'),
    'Lizard': ('Spock', 'Paper'),
    'Spock': ('Rock', 'Scissors')
}


def get_str_number():
    """

    Returns: Повертає номер останньої гри в історії програми

    """
    with open('gamehistory.txt', 'r') as file:
        return len(file.readlines()) + 1


def get_user_choice():
    """

    Функція отримує вибір користувача

    Returns: Повертає вибранний користувачем варіант

    """
    while True:
        user_choice = input('Choose your fighter! (Rock, Paper, Scissors, Spock, Lizard): ')
        if user_choice not in WIN_RULES.keys():
            print('Enter only: Rock, Paper, Scissors, Spock, Lizard')
            continue
        else:
            return user_choice


def get_computer_choice():
    """
    Функція отримує вибір комп'ютера

    Returns: Повертає вибранний комп'ютером варіант

    """
    computer_choice = choice(list(WIN_RULES.keys()))
    return computer_choice


def get_winner(user_choice, computer_choice):
    """
    Функція визначає переможця

    Args:
        user_choice: вибір користувача
        computer_choice: вибір комп'ютера

    Returns: Повертає повідомлення, в якому вказано переможця

    """

    if user_choice == computer_choice:
        winner = 'not defined'
    elif computer_choice in WIN_RULES[user_choice]:
        winner = 'User'
    else:
        winner = 'Computer'
    return winner


def log_maker(str_number, user_choice, computer_choice, winner):
    """

    Функція записує до окремого файлу історію результатів гри з часом

    Args:
        str_number: Номер строки
        user_choice: Вибір користувача
        computer_choice: Вибір комп'ютера
        winner: Переможець

    """
    date = time.strftime("%d %b %Y %X")

    with open('gamehistory.txt', 'a+') as file:

        file.readlines()

        if winner == 'not defined':
            message = f'{str_number}. {date}. User - {user_choice}, Computer - {computer_choice}. Draw\n'
        elif winner == 'User':
            message = f'{str_number}. {date}. User - {user_choice}, Computer - {computer_choice}. Winner - User\n'
        else:
            message = f'{str_number}. {date}. User - {user_choice}, Computer - {computer_choice}. Winner - Computer\n'

        file.write(f'{message}')


def make_message(user_choice, computer_choice, winner):
    """

    Args:
        user_choice: вибір користувача
        computer_choice: вибір комп'ютера
        winner: переможець раунду

    Returns: Повертає повне повідомлення про вибір гравців та результат гри

    """
    msg = f'Your choice is {user_choice}, PC choice is {computer_choice}. Winner is {winner}'
    return msg
