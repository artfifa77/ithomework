from gamelib import get_user_choice
from gamelib import get_computer_choice
from gamelib import get_winner
from gamelib import log_maker
from gamelib import make_message
from gamelib import get_str_number


def main():
    """

    Функція робить гру

    """
    print('Welcome to our fantastic game!')

    for i in range(1, 6):

        print(f'Round number {i}')

        user_choice = get_user_choice()

        computer_choice = get_computer_choice()

        winner = get_winner(user_choice, computer_choice)

        str_number = get_str_number()

        log_maker(str_number, user_choice, computer_choice, winner)

        msg = make_message(user_choice, computer_choice, winner)

        print(msg)

main()
