def str_maker(function):
    """

    Декоратор перетворює аргументи та результат функції у форматі "str"

    """
    def wrapped(*args, **kwargs):
        new_args = []

        for arg in args:
            arg = str(arg)
            new_args.append(arg)

        new_kwargs = {}

        for key, value in kwargs.items():
            key = str(key)
            value = str(value)
            dict_str = {key: value}
            new_kwargs.update(dict_str)

        function_action = function(*new_args, **new_kwargs)

        function_str = str(function_action)

        print('Output type:', type(function_str))
        return function_action
    return wrapped


@str_maker
def test(arg1, arg2, *args, **kwargs):
    """

    Функція для тесту декоратора

    """

    return print(arg1 + arg2)

test(1, 2)
