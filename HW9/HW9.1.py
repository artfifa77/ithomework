import time


def function_time(function):
    """

    Декоратор виводить час виконання функції

    """
    def wrapped(*args, **kwargs):
        start_time = time.time()

        res = function(*args, **kwargs)

        finish_time = time.time()

        print('Час виконання функції: ', finish_time - start_time)
        return res
    return wrapped


@function_time
def test_function():
    """

    Функція для тесту декоратора

    """
    for num in range(10000000):
        num_action = num/2

test_function()
