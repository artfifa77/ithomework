from HW8lib import get_user_choice
from HW8lib import get_computer_choice
from HW8lib import get_winner
from HW8lib import make_message


def main():
    """

    Функція робить гру

    """
    print('Welcome to our fantastic game!')

    for i in range(1, 6):
        print(f'Round number {i}')
        user_choice = get_user_choice()

        computer_choice = get_computer_choice()

        winner = get_winner(user_choice, computer_choice)

        msg = make_message(user_choice, computer_choice, winner)

        print(msg)

main()
