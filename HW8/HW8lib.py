from random import choice

WIN_RULES = {
    'rock': ('scissors', 'spock'),
    'paper': ('rock', 'spock'),
    'scissors': ('paper', 'lizard'),
    'lizard': ('spock', 'paper'),
    'spock': ('rock', 'scissors')
}


# get data from user
def get_user_choice():
    """

    Функція отримує вибір користувача

    Returns: Повертає вибранний користувачем варіант

    """
    while True:
        user_choice = input('Choose your fighter! (rock, paper, scissors, spock, lizard): ')
        if user_choice not in WIN_RULES.keys():
            print('Enter only: rock, paper, scissors, spock, lizard')
            continue
        else:
            return user_choice


# get data from computer
def get_computer_choice():
    """
    Функція отримує вибір комп'ютера

    Returns: Повертає вибранний комп'ютером варіант

    """
    computer_choice = choice(list(WIN_RULES.keys()))
    return computer_choice


# define winner
def get_winner(user_choice, computer_choice):
    """
    Функція визначає переможця

    Args:
        user_choice: вибір користувача
        computer_choice: вибір комп'ютера

    Returns: Повертає повідомлення, в якому вказано переможця

    """
    if user_choice == computer_choice:
        msg = 'Draw. Try again'
    elif computer_choice in WIN_RULES[user_choice]:
        msg = 'User. Congratulations!'
    else:
        msg = 'Computer. Don\'t give up!'

    return msg


# make message
def make_message(user_choice, computer_choice, winner):
    """

    Args:
        user_choice: вибір користувача
        computer_choice: вибір комп'ютера
        winner: переможець раунду

    Returns: Повертає повне повідомлення про вибір гравців та результат гри

    """
    msg = f'Your choice is {user_choice}, PC choice is {computer_choice}. Winner is {winner}'
    return msg
