# Доопрацюйте класс Point так, щоб в атрибути х та у обʼєктів цього класу можна було записати тільки числа.
# Використовуйте property


# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, що пишуться в точки аналогічно до класу Line.
# Визначет атрибут, що містить площу трикутника (за допомогою property).
# Для обчислень можна використати формулу Герона


class Point:
    _x = 0
    _y = 0

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x_coord):
        if not isinstance(x_coord, (int, float)):
            raise TypeError
        self._x = x_coord

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y_coord):
        if not isinstance(y_coord, (int, float)):
            raise TypeError
        self._y = y_coord


class Triangle:
    _first_point = (0, 0)
    _second_point = (0, 0)
    _third_point = (0, 0)

    def __init__(self, triangle_first_point, triangle_second_point, triangle_third_point):
        self._first_point = triangle_first_point
        self._second_point = triangle_second_point
        self._third_point = triangle_third_point

    @property
    def first_point(self):
        return self._first_point

    @first_point.setter
    def first_point(self, triangle_first_point):
        if not isinstance(triangle_first_point, Point):
            raise TypeError
        self._first_point = triangle_first_point

    @property
    def second_point(self):
        return self._second_point

    @second_point.setter
    def second_point(self, triangle_second_point):
        if not isinstance(triangle_second_point, Point):
            raise TypeError
        self._second_point = triangle_second_point

    @property
    def third_point(self):
        return self._third_point

    @third_point.setter
    def third_point(self, triangle_third_point):
        if not isinstance(triangle_third_point, Point):
            raise TypeError
        self._third_point = triangle_third_point

    @property
    def triangle_area(self):

        line1 = self.first_point.x * self.second_point.y - self.first_point.y * self.second_point.x

        line2 = self.second_point.x * self.third_point.y - self.second_point.y * self.third_point.x

        line3 = self.third_point.x * self.first_point.y - self.third_point.y * self.first_point.x

        return abs((line1 + line2 + line3) * 0.5)


point_1 = Point(2, 2)
point_2 = Point(6, 0)
point_3 = Point(6, 4)

triangle = Triangle(point_1, point_2, point_3)

print(triangle.triangle_area)
