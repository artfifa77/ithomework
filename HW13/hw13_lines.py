# Доопрацюйте класс Point так, щоб в атрибути х та у обʼєктів цього класу можна було записати тільки числа.
# Використовуйте property


# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, що пишуться в точки аналогічно до класу Line.
# Визначет атрибут, що містить площу трикутника (за допомогою property).
# Для обчислень можна використати формулу Герона


class Point:

    _x = 0
    _y = 0

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x_coord):
        if not isinstance(x_coord, int or float):
            raise TypeError
        self._x = x_coord

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y_coord):
        if not isinstance(y_coord, int or float):
            raise TypeError
        self._y = y_coord


class Line:
    _begin = None
    _end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, new_begin_point):
        if not isinstance(new_begin_point, Point):
            raise TypeError
        self._begin = new_begin_point

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, new_end_point):
        if not isinstance(new_end_point, Point):
            raise TypeError
        self._end = new_end_point

    @property
    def length(self):

        k1 = (self.begin.x - self.end.x) ** 2
        k2 = (self.begin.y - self.end.y) ** 2
        res = (k1 + k2) ** 0.5

        return res


class Triangle:
    _first_side = 0
    _second_side = 0
    _third_side = 0

    def __init__(self, triangle_first_side, triangle_second_side, triangle_third_side):
        self._first_side = triangle_first_side
        self._second_side = triangle_second_side
        self._third_side = triangle_third_side

    @property
    def first_side(self):
        return self._first_side

    @first_side.setter
    def first_side(self, triangle_first_side):
        if not isinstance(triangle_first_side, Line):
            raise TypeError
        self._first_side = triangle_first_side

    @property
    def second_side(self):
        return self._second_side

    @second_side.setter
    def second_side(self, triangle_second_side):
        if not isinstance(triangle_second_side, Line):
            raise TypeError
        self._second_side = triangle_second_side

    @property
    def third_side(self):
        return self._third_side

    @third_side.setter
    def third_side(self, triangle_third_side):
        if not isinstance(triangle_third_side, Line):
            raise TypeError
        self._first_side = triangle_third_side

    @property
    def triangle_area(self):
        half_perimeter = ((self.first_side.length + self.second_side.length + self.third_side.length) / 2)

        res = ((half_perimeter * (half_perimeter - self.first_side.length) *
                (half_perimeter - self.second_side.length) * (half_perimeter - self.third_side.length))) ** 0.5
        return res


p1 = Point(2, 2)
p2 = Point(6, 0)
p3 = Point(6, 4)

line = Line(p1, p2)
line2 = Line(p1, p3)
line3 = Line(p2, p3)

triangle = Triangle(line, line2, line3)

print(triangle.triangle_area)
